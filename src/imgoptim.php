<?php

class Imgoptim
{

	private $_arguments;

	private $_dsn;

	private $_ftpConnection;

	private $_psiMap;

	private $_eventListener = array();

	public function __construct(array $options)
	{
		$this->_arguments = array_merge(array(
			'collect' => false,
			'workdir' => sprintf('%s/.files', dirname(dirname(__FILE__))),
			'check_permissions' => true,
			'simulate_upload' => true,
			'check_corrupt' => true,
			'keep_original' => true,
			'algorithm_jpg' => array(
				'jpegtran',
				'jpegoptim'
			),
			'jpg_quality' => 100
		), $options);
	}

	public function addEventListener($event, $callback)
	{
		if(!isset($this->_eventListener[$event]))
			$this->_eventListener[$event] = array();
		$this->_eventListener[$event][] = $callback;
	}

	protected function _dispatchEvent($event, array $arguments)
	{
		if(!isset($this->_eventListener[$event]))
			return;

		foreach($this->_eventListener[$event] as $listener)
		{
			if(!is_callable($listener))
				continue;

			try
			{
				call_user_func_array($listener, $arguments);
			}
			catch(Exception $e)
			{
				$this->log($e->getMessage());
			}
		}
	}

	public function run()
	{
		foreach($this->findAllFiles() as $file)
		{
			$this->compress($file);
		}
	}

	public function findAllFiles()
	{
		$dsn = $this->parseDsn($this->_arguments['dsn']);
		return $this->_findAllFilesRecursive($this->getFtpConnection(), $dsn['path']);
	}

	public function checkPermission($permission, $user)
	{
		if(!$this->_arguments['check_permissions'])
			return true;

		$dsn = $this->parseDsn($this->_arguments['dsn']);
		$data = array(
			'user' => substr($permission, 1, 3),
			'group' => substr($permission, 4, 3),
			'other' => substr($permission, 7, 3)
		);
		if($user == $dsn['username'])
		{
			unset($data['group'], $data['other']);
		}
		else
		{
			unset($data['user']);
		}

		foreach($data as $rwx)
		{
			if(substr($rwx, 1, 1) == 'w')
				return true;
		}
		return false;
	}

	public function log($message)
	{
		printf("%s\n", $message);
	}

	private function _findAllFilesRecursive($ftpConnection, $path)
	{
		$list = array();
		$this->log(sprintf('scanning directory %s', $path));
		$contents = ftp_rawlist($ftpConnection, $path);

		if(!$contents)
			return $list;

		$this->_dispatchEvent('finder.begin', array(
			'path' => $path
		));

		foreach($contents as $line)
		{
			$data = preg_split("/\s+/", $line);
			$perm = reset($data);
			$currentFile = end($data);

			if(substr($currentFile, 0, 1) == '.')
				continue;

			if(!$this->checkPermission($perm, $data[2]))
			{
				$this->log(sprintf('file %s skipped due permissions: %s to user %s', $currentFile, $perm, $data[2]));
				continue;
			}

			$currentPath = $path . DIRECTORY_SEPARATOR . $currentFile;

			if(substr($perm, 0, 1) === 'd')
			{
				$list = array_merge($list, $this->_findAllFilesRecursive($ftpConnection, $currentPath));
			}
			else
			{
				if(in_array($this->getSuffix($currentFile), $this->getValidSuffixes()))
				{
					if(!$this->_arguments['collect'])
						$this->compress($currentPath, $data);
					else
					{
						$list[] = $currentPath;
						$this->_dispatchEvent('finder.enqueue', array(
							'filename' => $currentPath,
							'filesize' => $data[4]
						));
					}
				}
			}
		}

		$this->_dispatchEvent('finder.end', array(
			'path' => $path
		));
		return $list;
	}

	public function getValidSuffixes()
	{
		return array(
			'png',
			'jpg',
			'jpe',
			'jpeg'
		);
	}

	public function getSuffix($filename)
	{
		$tmp = explode('.', $filename);
		return strtolower(end($tmp));
	}

	public function stat($filename, $result)
	{
		//
	}

	private function _getFtpFilemode($filename)
	{
		$mode = FTP_BINARY;
		if(in_array($this->getSuffix($filename), array(
			'css',
			'js'
		)))
		{
			$mode = FTP_ASCII;
		}
		return $mode;
	}

	public function download($filename, $localFilename)
	{
		$result = ftp_get($this->getFtpConnection(), $localFilename, $filename, $this->_getFtpFilemode($filename));
		if($this->_arguments['keep_original'])
		{
			copy($localFilename, sprintf('%s.backup.%s', $localFilename, $this->getSuffix($filename)));
		}
		return $result;
	}

	public function upload($localFilename, $filename)
	{
		if($this->_arguments['simulate_upload'])
			return true;
		return ftp_put($this->getFtpConnection(), $filename, $localFilename, $this->_getFtpFilemode($filename));
	}

	public function compress($filename, array $info = null)
	{
		$dsn = $this->parseDsn($this->_arguments['dsn']);
		$localFilename = implode(DIRECTORY_SEPARATOR, array(
			$this->_arguments['workdir'],
			$dsn['host'],
			sprintf('%s.%s', md5($filename), $this->getSuffix($filename))
		));
		if(!is_dir(dirname($localFilename)))
			mkdir(dirname($localFilename), 0755, true);

		if(file_exists($localFilename))
		{
			$this->log(sprintf('skip, local file "%s" exists', $localFilename));
			$this->_dispatchEvent('compress.skip', array(
				'filename' => $filename,
				'local_filename' => $localFilename
			));
			return;
		}

		if($this->download($filename, $localFilename))
		{
			$result = null;
			switch ($this->getSuffix($filename))
			{
				case 'jpg':
				case 'jpe':
				case 'jpeg':
					$result = $this->compressJpg($localFilename, $filename);
					break;

				case 'png':
					$result = $this->compressPng($localFilename, $filename);
					break;

				case 'css':
					$result = $this->compressCss($localFilename, $filename);
					break;

				case 'js':
					$result = $this->compressJs($localFilename, $filename);
					break;
			}

			if($result && $result['compressed'])
			{
				if($this->upload($localFilename, $filename))
				{
					$this->stat($filename, $result);
					$this->log(sprintf('%s bytes saved on file %s (old: %d, new: %d)', $result['compressed'], $filename, $result['origin'], $result['new']));
					$this->_dispatchEvent('compress.success', array(
						'filename' => $filename,
						'result' => $result
					));
				}
				else
				{
					$this->log(print_r($info, true));
				}
			}
			else
			{
				$this->log(sprintf('no compression for %s', $filename));
				$this->_dispatchEvent('compress.none', array(
					'filename' => $filename,
					'filesize' => filesize($localFilename)
				));
			}
			if(file_exists($localFilename))
				unlink($localFilename);
		}
	}

	protected function _compress($localFilename, $filename, $callback, $loop = false)
	{
		if($this->_useExternalCompressedFile($localFilename, $filename))
			return;
		
		try
		{
			$runs = 0;
			$origin = null;
			do
			{
				$before = filesize($localFilename);
				if(!$before)
				{
					$this->log(sprintf('skipping, file %s is empty', $filename));
					return false;
				}
				if(is_null($origin))
					$origin = $before;

				$this->log(sprintf('processing %s (%d) with %d bytes', $filename, ++$runs, $before));
				$callback($localFilename, $filename);
				clearstatcache();
				$compressed = $before - ($nfs = filesize($localFilename));
				if(!$nfs)
				{
					$this->log('something went wrong!');
					return false;
				}
				if($compressed < 0)
					$compressed = 0;
			}
			while($loop && $compressed);

			return array(
				'compressed' => $origin - $nfs,
				'origin' => $origin,
				'new' => $nfs
			);
		}
		catch(Exception $e)
		{
			$this->log(sprintf('an error eccured while compressing: "%s"', $e->getMessage()));
			$this->_dispatchEvent('compress.error', array(
				'filename' => $filename,
				'error' => $e->getMessage()
			));
			return false;
		}
	}

	public function compressCss($localFilename, $filename)
	{
		return $this->_compress($localFilename, $filename, function ($localFilename, $filename)
		{
			$cmd = sprintf('cssnano %1$s %1$s', $localFilename);
			shell_exec($cmd);
		});
	}

	public function compressJs($localFilename, $filename)
	{
		return $this->_compress($localFilename, $filename, function ($localFilename, $filename)
		{
		});
	}

	public function compressJpg($localFilename, $filename)
	{
		$quality = $this->_arguments['jpg_quality'];

		$lossy = $quality < 100;
		return $this->_compress($localFilename, $filename, function ($localFilename, $filename) use ($lossy, $quality)
		{
			if($this->_arguments['check_corrupt'])
			{
				$cmd = sprintf('%s/jpeg_corrupt.py %s 2>&1', dirname(__FILE__), $localFilename);
				if($output = trim(shell_exec($cmd)))
					throw new \Exception($output);
			}
			if($this->_arguments['algorithm_jpg'] && in_array('jpegtran', $this->_arguments['algorithm_jpg']))
			{
				$cmd = sprintf('jpegtran -copy none -optimize -progressive -outfile %1$s %1$s 2>&1', $localFilename);
				if($output = trim(shell_exec($cmd)))
					throw new \Exception($output);
			}
			if($this->_arguments['algorithm_jpg'] && in_array('jpegoptim', $this->_arguments['algorithm_jpg']))
			{
				$arguments = [
					'--strip-exif',
					'--strip-iptc',
					'--strip-com',
					'--force'
				];
				if($lossy)
					$arguments[] = sprintf('--max=%d', $quality);
				$cmd = sprintf('jpegoptim %s %s', implode(' ', $arguments), $localFilename);
				shell_exec($cmd);
			}
			if($this->_arguments['check_corrupt'])
			{
				$cmd = sprintf('%s/jpeg_corrupt.py %s 2>&1', dirname(__FILE__), $localFilename);
				if($output = trim(shell_exec($cmd)))
					throw new \Exception($output);
			}
		}, !$lossy);
	}

	public function compressPng($filename)
	{
		$before = filesize($filename);
		$cmd = sprintf('pngquant --force %s', $filename);
		shell_exec($cmd);
		$newFilename = str_replace('.png', '-fs8.png', $filename);
		if(file_exists($newFilename))
		{
			$after = $before - ($nfs = filesize($newFilename));
			if($after < 0)
				$after = 0;
			unlink($filename);
			rename($newFilename, $filename);
			return array(
				'compressed' => $after,
				'origin' => $before,
				'new' => $nfs
			);
		}
		return false;
	}

	protected function _useExternalCompressedFile($localFilename, $filename)
	{
		return $this->_usePsiCompressedFile($localFilename, $filename);
	}

	protected function _usePsiCompressedFile($localFilename, $filename)
	{
		if(!isset($this->_arguments['psi']) || $this->_arguments['psi'])
			return false;

		if(is_null($this->_psiMap))
		{
			$basedir = rtrim($this->_arguments['basedir'], DIRECTORY_SEPARATOR);
			if(!$basedir)
				throw new \Exception('basedir missing');

			$domain = rtrim($this->_arguments['domain'], DIRECTORY_SEPARATOR);
			if(!$domain)
				throw new \Exception('domain missing');

			$this->_psiMap = array();
			foreach(explode(PHP_EOL, file_get_contents($this->_arguments['psi'] . '/MANIFEST')) as $line)
			{
				if(false === strpos($line, ':'))
					continue;
				list ($k, $v) = explode(':', $line, 2);

				$v = trim($v);

				if(substr($v, 0, strlen($domain)) != $domain)
					continue;

				$v = str_replace($domain, $basedir, $v);

				$this->_psiMap[$v] = trim($k);
			}
		}

		if(isset($this->_psiMap[$filename]))
		{
			copy($this->_arguments['psi'] . DIRECTORY_SEPARATOR . $this->_psiMap[$filename], $localFilename);
			return true;
		}
	}

	public function getFtpConnection()
	{
		if(is_null($this->_ftpConnection))
		{
			$dsn = $this->parseDsn($this->_arguments['dsn']);
			if(!($dsn['host']))
				throw new Exception('ftp-server not given');

			$this->log(sprintf('login to %s', $dsn['host']));

			$connect = function ($host, $ssl = true)
			{
				if(!($id = $ssl ? ftp_ssl_connect($host) : ftp_connect($host)))
					throw new Exception('ftp-connect with ssl failed');
				return $id;
			};

			$ssl = false;
			try
			{
				$conn_id = $connect($dsn['host'], true);
				$ssl = true;
			}
			catch(Exception $e)
			{
				if(!($conn_id = $connect($dsn['host'])))
					throw new Exception('ftp-connect with failed');
			}

			try
			{
				if(!ftp_login($conn_id, $dsn['username'], $dsn['password']))
					throw new Exception('ftp-login failed');
			}
			catch(Exception $e)
			{
				$conn_id = $connect($dsn['host'], !$ssl);
				if(!ftp_login($conn_id, $dsn['username'], $dsn['password']))
					throw new Exception('ftp-login failed');
			}

			ftp_pasv($conn_id, true);
			ftp_chdir($conn_id, $dsn['path']);

			$this->log(sprintf('connection established, path is %s', $dsn['path']));

			$this->_ftpConnection = $conn_id;
		}
		return $this->_ftpConnection;
	}

	public function parseDsn($dsn)
	{
		if(is_null($this->_dsn))
		{
			list ($type, $dsn) = explode('://', $dsn, 2);
			switch ($type)
			{
				case 'ssh':
					$port = 22;
					break;

				default:
					$port = null;
					break;
			}

			list ($host, $local) = explode('/', $dsn, 2);
			$local = '/' . $local;

			$tmp = explode('@', $host);
			$host = array_pop($tmp);
			$access = implode('@', $tmp);

			$password = null;
			if(strpos($access, ':') !== false)
			{
				list ($access, $password) = explode(':', $access, 2);
			}
			else
			{
				if($input = readline('enter password (leave empty for non-password-login):'))
					$password = trim($input);
			}

			if(strpos($host, ':') !== false)
				list ($host, $port) = explode(':', $host);

			$this->_dsn = [
				'protocol' => $type,
				'username' => $access,
				'password' => $password,
				'host' => $host,
				'port' => $port,
				'path' => $local
			];
		}
		return $this->_dsn;
	}
}