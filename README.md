# imgoptim

usage:

`php scripts/imgoptim --dsn=ftp://user:passwort@host/path/to/dir`

durchläuft das verzeichnis rekursiv, und versucht jedes gefundene bild möglichst verlustfrei zu komprimieren

### Google PageSpeed Insights (psi)

Googles PageSpeed Insights wird nativ unterstützt. Das heruntergeladene Zip-Archiv kann verwendet werden:

~~~
php scripts/imgoptim --dsn=ftp://user:passwort@host/path/to/shop/media --domain=http://example.com --basedir=/path/to/shop --psi=/Users/me/Download/optimizedContents 
~~~

Dabei ist `/Users/me/Download/optimizedContents` der Pfad zum entpackten Zip-Archiv, der von PSI angeboten wird.